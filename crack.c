#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 34;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char * targetHash, char * dictionaryFilename)
{
    // Open the dictionary file
    FILE *fp = fopen(dictionaryFilename, "r");

    if (!fp)
    {
        fprintf(stderr, "Can't open %s for reading ", dictionaryFilename);
        perror(NULL);
        exit(1);
    }

    char *word = malloc(PASS_LEN);

    // Loop through the dictionary file, one line
    // at a time.
    while( fgets(word, PASS_LEN, fp) != NULL)
    {
        // Hash each password. Compare to the target hash.
        char *hash = md5(word, strlen(word));
        // If they match, return the corresponding password.
        if (strcmp(hash, targetHash) == 0)
        {
            return word;
        }
    }

    if(fp) 
    {
        fclose(fp);
    }
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *fp = fopen("hashes.txt", "r");

    if (!fp)
    {
        fprintf(stderr, "Can't open hashes.txt for reading ");
        perror(NULL);
        exit(1);
    }

    char hasher[HASH_LEN];
    // For each hash, crack it by passing it to crackHash
    while( fgets(hasher, HASH_LEN, fp) != NULL)
    {
        char *nl = strchr(hasher, '\n');
		if (nl) *nl = '\0';
        char *cracked = crackHash(hasher, argv[2] );
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        printf("%s %s\n", hasher, cracked);

        
        //Free memory
        free(cracked);
    }

    //Close file if still open
    if(fp) 
    {
        fclose(fp);
    }
    
    return 0;
}
